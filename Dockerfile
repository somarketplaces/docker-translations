FROM buildpack-deps:precise

ENV DEBIAN_FRONTEND noninteractive

RUN locale-gen en_US.UTF-8 \
  && dpkg-reconfigure locales

ENV LANG en_US.UTF-8

RUN apt-get update \
  && apt-get install -y --no-install-recommends apt-utils \
  && rm -rf /var/lib/apt/lists/*

RUN cd /root \
  && wget http://ftp.gnu.org/pub/gnu/libiconv/libiconv-1.14.tar.gz \
  && tar -xzvf libiconv-1.14.tar.gz \
  && cd libiconv-1.14 \
  && ./configure --prefix=/usr \
  && make \
  && make install \
  && rm -rf /root/ibiconv-1.14.tar.gz /root/ibiconv-1.14

RUN apt-get update \
  && apt-get -y --no-install-recommends install gettext python python-pip \
  && rm -rf /var/lib/apt/lists/*
  
RUN pip install awscli --ignore-installed six \
  && rm -rf ~/.cache/pip

RUN set -x \
  && iconv --version \
  && python --version \
  && aws --version \
  && md5sum --version \
  && msgexec --version \
  && msgfmt --version
